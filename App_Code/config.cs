﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// config 的摘要说明
/// </summary>
public class config
{
    public config()
    {
        //
        // TODO: 在此处添加构造函数逻辑
        //
    }
    // 应用ID,您的APPID
    public static string app_id = "2021000118642326";

    // 支付宝网关
    public static string gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 商户私钥，您的原始格式RSA私钥
    public static string private_key = "MIIEogIBAAKCAQEAkpPI80VOxk6dNOPLB+gDrRbrKgoPMR3p0hySlKiliqRoZxwADrVzJBrTlodQQzACYBnpyXKV/8IfW089kQaHW084io0O9OCRbGiKtT6kvdJ9v2+QfhFYZLueoGVw0T7FlAhDKfNKvX6cmXWNCi6OkNRVHhyCnZS7anKmu3IS1paqeKBsar1MyOCFk9nELbf96Z6CvWti6lZJtFuMo2bV2UT2j0wFPhT1Z+K2wA6BQLiNb8aZPZwtmuS2GNJf8ci8Q+BSORohI1MFZi/ukzqM2oDowMvB/A8/GDTXu11LQcOQakdunJZHi/VZ+GvOIWCyjqI9/Bj8PUbxqYUV3JHMGwIDAQABAoIBAG3RNXQ2EA/5ZET1Ce9xgBeVnTkQ8lLdU4qtB5FDTUuzY9kknFJo1cxOIQqddJ/x/0WbBJJ+PC2zdiTGGJ1oY88gan87seH7wliTdqDWPz1M0BO2dW5Odoh6HR2VAtJykjsXeaKbefrBOnirHrFrMjsGKoN13tJLVaMqJQs5Ky4D7YH411Lf49rOhw+gsYSaNYvvZx5YGLbp/FwDXlA6iTxoavqiz7HMJrCa0yh64ZmRHVdsZ1Cwz/brig3TTSN7UDuTBvDcEr9M086IfbKeenoayn9w5BaQ23xrfPK8rKHY/T+Yx/PSoKUFhwfvY1BnpiQh1eyEi/s2LSSq5SvGh5kCgYEA2iURsm46qn16lwYPy7Qi7B3Fw69Jc1CnGLcoWlJO4voQ3S7SpoyCrmD42P57rBDxSfrCOG763kTspVfZ4Xc29PPjNuI99sQKWRk8oU8Hd664AufqPV15wEWABgSWcaojO3F1Xd9oSZ5qoBV3gL8b6oF1pZEvxMnlXXoKOPKSNcUCgYEArANg9sUI8lUP/Twl8gt62qJVyYfrVQSb/jEKqcgHvfKNs1P2U3HFiVOEMbt5KO1sr4UA9upggpVFirOLHs7Hi07dy6sui3HnhQSztrwRWsAn86A7SRd/BMiWKmoJotcNrzlYf7cvoGfVpbelks5RiAsharT27Rfzv99K2F/l+F8CgYAb7T70vtPX27SD/QysoWUPIUieIqSelKavrVMHLR5YRHdAOaxSEg7w0A1Lili+0HRzahzPKLYJGn3JNRP5n09z0H04G4KfQr2oZmiypfFoyv9Xr+T28i6SBNO88X1pE8Tq6T2DRQSO2C8CS30/CDhEr0omZg3dfZNVgjgozjUVmQKBgB3GN/J/CJ1pu+rJjofnFMzjbJYFwOZmddn6Ay6AQVbPtVdlIS4wc1Dh8+/n04ESF84J57DVwP7Zd8Aac+pRQJkMfbfTWaspfDm6JqFqzFY5DLFO+vQb7RCW5+dPk3tsXiIGexsgSk4pqwGUWEShEdvKkW43BMblbnu32uy5LMVhAoGAPyfLWxv6sHkf3Lf7deIWU6tg8HkyQXSS+2XG3Cy/+9uB7Rl0Q9LqWgzRMFdmeFkOoO0cnVUUVjBurvIezbV8PMadBLQ+gsB8IVgTfJNFV3er9MQVKT64hnSc6sMKfe0MVyRAOorMeFrjsx1qAnnCZncdlJuT4vcXCKBKUaGHpvA=";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static string alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjSldFOK8+6qyBaT5fEg/Gne4fkuvpn8CeYBeG9Z4LMxjBfmkJ2PRPX24LJ9oKnwQb+riuhS8LvJ366fH4efF2nwzKn4ZViJI8YqWyvHu/DO+6o9tYBJZv4mgF7VZvBk/qK9biQFZQMy+bZFZ3lFoFrLwkW06baa/i4Zwnl/fD+fNUkFwdB34sBsURyw5bbm7UObg0NQ4lorCqfxoS6ZLwYUuut0Ix14b1f7UWCwEbzSU9I7rprsj3io2/uzRiN/SRLPEcvaa73/e9ZjaxwTJE5BKXceA8bTGjHxGxf3dgcdIbzXZW8deCV5YkhXkzvJtZyrce0zBSdY40Embq3ksBQIDAQAB";

    // 签名方式
    public static string sign_type = "RSA2";

    // 编码格式
    public static string charset = "UTF-8";
}